//Leer un número entero de dos digitos y determinar si es primo y además si es negativo.

function isPrime(number){

    endStep = number/2;
    let i = 2; 
    while(i <= endStep){

        if(number % i == 0){
            
            return false;
        }

        i = i + 1;
    }

    return true;
}
function primeNumberTwoDigitis(number){

    let result

    if(number > 1 && number < 100){

        test = isPrime(number);
    
        if(test == true){
            result = `El numero ${number} es primo`;
        }else{
            result = `El numero ${number} no es primo`;
        }

    }else if(number < 0){

        result =  `El numero ${number} es negativo, pero no es primo ya que la definicion de numero primo no incluye a los negativos!`;

    }else{

        result =  `Se debe ingresar un numero positivo mayor que 1 y menor que 100 `;

    }

    return result;
}

let oneNumber = 97;

result = primeNumberTwoDigitis(oneNumber);

console.log(result);