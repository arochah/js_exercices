
// Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo. 

function splitNumberFourDigities(number){

    let partialNumber = number.toString();

    let digitOne = partialNumber[0].toString();
    let digitTwo = partialNumber[1].toString();
    let digitThree = partialNumber[2].toString();
    let digitFour = partialNumber[3].toString();

    return [digitOne, digitTwo, digitThree, digitFour];

}

function someEqualDigities(number){

    let result

    if(number > 999 && number < 10000){

        let digities = splitNumberFourDigities(number);

        if( digities[2] == digities[1]){

            result = `En ${number}, el penultimo digito es igual al segundo`

        }else{

            result = `En ${number}, el penultimo digito no es igual al segundo`

        }


    }else{

        result = `Se debe ingresar un numero de 4 cifras`;

    }

    return result;

}

let oneNUmber = 3226;

let result = someEqualDigities(oneNUmber);

console.log(result);