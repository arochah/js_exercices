
//Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentra el número mayor. 
function askData(number){

    let out;

    if (typeof(number) == "number"){

        out = number

    }else{
        out = false;
    }

    return out;

}

function arrayNumbers(){

    let array = [];

    for (var i = 0; i < arguments.length; i++){

        let isNumber = askData(arguments[i]);

        if (isNumber == false){ 

            array = "Uno de los valores ingresados no es un numero!!"
            break;

        }else{

            array.push(isNumber)
        }
    }

    return array;

}

function biggest(numbers){

    let biggestNumber = 0;
    let positionBiggest;
    let answer;

    if(typeof(numbers) == "string"){

        answer = numbers;

    }else{

        for(let i = 0; i < numbers.length; i++){

            if(numbers[i] > biggestNumber){
    
                biggestNumber = numbers[i];
                positionBiggest = i;
    
            }
    
        }

        answer = `De los numeros ingresados el mas grande es ${biggestNumber}, en la posicion ${positionBiggest}`;

    }

    return answer;
}


let numbers = arrayNumbers(22, 45, 6, 9, 12, 31, 29, 52, 9, 10);

let result = biggest(numbers);

console.log(result)