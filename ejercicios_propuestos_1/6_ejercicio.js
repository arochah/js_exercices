
// Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.

function digits(number){

    let firstDigit = parseInt(number/10);
    let secondDigit = number % 10;

    return [firstDigit, secondDigit];

}

function isMultiple(number){

    let result

    if(number > 9 && number < 100){

        let digitsOfNumer = digits(number);

        if(digitsOfNumer[0] % digitsOfNumer[1] == 0){

            result = `En el numero ${number}, ${digitsOfNumer[0]} es multiplo de ${digitsOfNumer[1]}`

        }else if(digitsOfNumer[1] % digitsOfNumer[0] == 0){

            result = `En el numero ${number}, ${digitsOfNumer[1]} es multiplo de ${digitsOfNumer[0]}`

        }else{

            result = `los digitos que conforman ${number} no son multiplos entre si`

        }

    }else{

        result = `Se debe ingresar un numero de 2 cifras`;

    }

    return result;

}

let oneNumber = 28;

result = isMultiple(oneNumber);

console.log(result);