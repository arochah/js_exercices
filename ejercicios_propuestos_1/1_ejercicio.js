
//Leer un número entero y determinar si es negativo.
function positiveOrNegative(number)  {

    let result

    if(typeof(number) == `number`) {

        
        if(number < 0) {

            result = `El numero ${number} es negativo!`;

        }else{

            result = `El numero ${number} es positivo!`;

        }
    }else {

        result = `Se debe ingresar un numero`;

    }

    return result;
};

let oneNumber = 99;

result = positiveOrNegative(oneNumber);

console.log(result);

