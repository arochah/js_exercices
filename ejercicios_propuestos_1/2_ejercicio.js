
//Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos.

function sumTwoDigitis(number){

    let result 

    if((number > -100 && number < -9) || (number < 100 && number > 9)){

        let firstDigit = parseInt(number/10);
        let secondDigit = number % 10;

        result = (firstDigit + secondDigit);

    }else{

        result = `El valor ingresado debe ser un numero de 2 cifras`
    
    }

    return result;
}

let oneNumber = 28;

result = sumTwoDigitis(oneNumber);

console.log(result)