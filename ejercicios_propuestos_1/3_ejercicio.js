//Leer un número entero de dos digitos y determinar si ambos digitos son pares.


function twoDigitisEven(number){

    let result 

    if((number > -100 && number < -9) || (number < 100 && number > 9)){
        
        let firstDigit = parseInt(number/10);
        let secondDigit = number % 10;

        if((firstDigit % 2 == 0) && (secondDigit % 2 == 0)){

            result = `Los 2 digitos de ${number} son pares!`;

        }else{

            result = `El numero ${number} no esta formado por digitos pares`;

        }

    }else{

        result = `El valor ingresado debe ser un numero de 2 cifras`
    
    }

    return result;

}

let oneNumber = 24;

result = twoDigitisEven(oneNumber);

console.log(result);
