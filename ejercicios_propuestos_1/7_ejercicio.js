
//Leer un número entero de dos digitos y determinar si los dos digitos son iguales.

function digits(number){

    let firstDigit = parseInt(number/10);
    let secondDigit = number % 10;

    console.log(firstDigit)
    return [firstDigit, secondDigit];

}

function equalDigitis(number){

    let result 

    if(number > 9 && number < 100){

        let digitsOfNumer = digits(number);

        if(digitsOfNumer[0] == digitsOfNumer[1]){

            result = `Los digitos de ${number} son iguales`;

        }else{

            result = `Los digitos de ${number} no son iguales`;

        }

    }else{

        result = `Se debe ingresar un numero de 2 cifras`;

    }

    return result;

}

let oneNumber = 22;

result = equalDigitis(oneNumber);

console.log(result);