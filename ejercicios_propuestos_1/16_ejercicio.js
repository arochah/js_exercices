
//Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentran los números terminados en O
function askData(number){

    let out;

    if (typeof(number) == "number"){

        out = number

    }else{
        out = false;
    }

    return out;

}
function arrayNumbers(){

    let array = [];

    for (var i = 0; i < arguments.length; i++){

        let isNumber = askData(arguments[i]);

        if (isNumber == false){ 

            array = "Uno de los valores ingresados no es un numero!!"
            break;

        }else{

            array.push(isNumber)
        }
    }

    return array;

}

function lastNumerCero(numbers){

    let answer;
    let positionCero = [];

    if(typeof(numbers) == "string"){

        answer = numbers;

    }else{

        for(let i = 0; i < numbers.length; i++){

            let strNumber = numbers[i].toString();

            if(strNumber[(strNumber.length - 1)] == '0'){

                positionCero.push(i)

            }
        }

        answer = `Las posiciones de los numeros que terminan en 0 son ${positionCero}`;

    }

    return answer;

}

let numbers = arrayNumbers(23, 10, 12, 50, 100, 16, 21, 1, 92, 250)

let result = lastNumerCero(numbers)

console.log(result)