
// Leer un número entero y determinar a cuánto es igual la suma de sus digitos.
askData = function (number){

    let out;

    if (typeof(number) == "number"){

        out = number

    }else{
        out = false;
    }

    return out;

}

splitTheNumber = function (number){

    let out = [];

    let strNumber = number.toString();

    for (let i = 0; i < strNumber.length ; i++){

        out.push(strNumber[i]);

    }

    return out;
}

sumDigits = function (number){

    let totalSum = 0;

    for (let i = 0; i < number.length; i++){

        totalSum += parseInt(number[i]);

    }

    return totalSum;

}


generalProcess = function (number){

    let result;

    number = askData(number);

    if (typeof(number) == "number"){

        strNumber = splitTheNumber(number)

        answer = sumDigits(strNumber)

        result = `Los digitos de ${number} suman : ${answer}`;

    }else{

        result = `El valor ingresado no es un numero`;

    }

    return result;
}

module.exports ={
    askData,
    splitTheNumber,
    sumDigits,
    generalProcess
}