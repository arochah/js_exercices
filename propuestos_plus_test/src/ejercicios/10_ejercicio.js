
// Leer dos numeros enteros y determinar si la diferencia entre los dos es un número divisor exacto de alguno de los dos números.

askData = function (number1, number2){

    let out;

    if (typeof(number1) == "number" && typeof(number2) == "number"){

        out = [number1, number2];

    }else{
        out = false;
    }

    return out;

}

isDivisor = function (number, divisor){

    let answer;

    if(number % divisor == 0){

        answer = ` ES divisor de ${number}`

    }else{

        answer = ` NO es divisor de ${number}`

    }

    return answer;

}

testOfCondition = function(numbers){

    let result;

    let dif = Math.abs(( numbers[0] - numbers[1]));

    let answer1 = isDivisor(numbers[0], dif);
    let answer2 = isDivisor(numbers[1], dif);

    result = `La diferencia de los numeros es ${dif},${answer1} y${answer2}`;

    return result;

}

isTheCase = function (number1, number2){

    let result;

    let numbers = askData(number1, number2);

    if (typeof(numbers) != "object"){

        result = `Los valores ingresados, o almenos uno de ellos no es un valor numerico.`

    }else{

        result = testOfCondition(numbers)

    }

    return result;

}

module.exports = {
    askData,
    isDivisor,
    testOfCondition,
    isTheCase
}