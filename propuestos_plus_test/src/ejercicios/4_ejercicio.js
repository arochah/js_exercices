
//Leer un número entero de dos digitos menor gue 20 y determinar si es primo.

isPrime = function (number){

    endStep = number/2;
    let i = 2; 
    while(i <= endStep){

        if(number % i == 0){
            
            return false;
        }

        i = i + 1;
    }

    return true;
}

primeNumberDowntwenty = function (number){

    let result 

    if(number > 9 && number < 20){

        test = isPrime(number);
    
        if(test == true){
            result = `El numero ${number} es primo`;
        }else{
            result = `El numero ${number} no es primo`;
        }

    }else{
        result = `Se debe ingresar un numero positivo menor que 20 y mayor que 9`;
    }

    return result;
}

module.exports = {
    isPrime,
    primeNumberDowntwenty
}
