
//Leer un número entero y mostrar en pantalla su tabla de multiplicar
askData = function (number){

    let out;

    if (typeof(number) == "number"){

        out = number

    }else{
        out = false;
    }

    return out;

}

tableOfOneNumber = function (number){

    let answer;
    number = askData(number);

    if(typeof(number) == "number"){

        console.log(`Esta es la tabla del ${number}`);

        for(let i = 1; i < 11 ; i++){
    
            console.log(`${number} X ${i} = ${number * i}`);
    
        }

        answer = `Gracias por usar este generador de tablas de multiplicar inprovisado`;
    
    }else{
        answer = `Asegurese de ingresar un numero`
    }

    return answer;

}

module.exports ={
    askData,
    tableOfOneNumber
}