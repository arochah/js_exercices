
//Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentran los números terminados en O
askData = function (number){

    let out;

    if (typeof(number) == "number"){

        out = number

    }else{
        out = false;
    }

    return out;

}
arrayNumbers = function (){

    let array = [];

    for (var i = 0; i < arguments.length; i++){

        let isNumber = askData(arguments[i]);

        if (isNumber == false){ 

            array = "Uno de los valores ingresados no es un numero!!"
            break;

        }else{

            array.push(isNumber)
        }
    }

    return array;

}

lastNumerCero = function (numbers){

    let answer;
    let positionCero = [];

    if(typeof(numbers) == "string"){

        answer = numbers;

    }else{

        for(let i = 0; i < numbers.length; i++){

            let strNumber = numbers[i].toString();

            if(strNumber[(strNumber.length - 1)] == '0'){

                positionCero.push(i)

            }
        }

        answer = `Las posiciones de los numeros que terminan en 0 son ${positionCero}`;

    }

    return answer;

}

module.exports = {
    askData,
    arrayNumbers,
    lastNumerCero
}