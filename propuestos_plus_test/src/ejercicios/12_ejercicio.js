
//Leer un número entero y determinar cuantas veces tiene el digito 1
askData = function (number){

    let out;

    if (typeof(number) == "number"){

        out = number

    }else{
        out = false;
    }

    return out;

}
splitTheNumber = function (number){

    let out = [];

    let strNumber = number.toString();

    for (let i = 0; i < strNumber.length ; i++){

        out.push(strNumber[i]);

    }

    return out;
}

isOne = function (number){

    let totalOne = 0;

    for (let i = 0; i < number.length; i++){

        if(parseInt(number[i]) == 1){
        
            totalOne += parseInt(number[i])
        
        }

    }

    return totalOne;

}

generalProcess = function (number){

    let result;

    number = askData(number);

    if (typeof(number) == "number"){

        strNumber = splitTheNumber(number)

        answer = isOne(strNumber)

        result = `En el numero ${number} tiene el numero 1 la siguiente cantidad de veces : ${answer}`;

    }else{

        result = `El valor ingresado no es un numero`;

    }

    return result;
}

module.exports = {
    askData,
    splitTheNumber,
    isOne,
    generalProcess
}