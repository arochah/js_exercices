
//Leer 10 números enteros, almacenados en un vector y determinar cuántas veces está repetido el mayor

askData = function (number){

    let out;

    if (typeof(number) == "number"){

        out = number

    }else{
        out = false;
    }

    return out;

}

arrayNumbers = function (){

    let array = [];

    for (var i = 0; i < arguments.length; i++){

        let isNumber = askData(arguments[i]);

        if (isNumber == false){ 

            array = "Uno de los valores ingresados no es un numero!!"
            break;

        }else{

            array.push(isNumber)
        }
    }

    return array;

}

biggest = function (numbers){

    let biggestNumber = 0;
    let positionBiggest = [];
    let answer;

    if(typeof(numbers) == "string"){

        answer = numbers;

    }else{

        for(let i = 0; i < numbers.length; i++){

            if(numbers[i] > biggestNumber){
    
                biggestNumber = numbers[i];
                positionBiggest = [];
                positionBiggest.push(i);
    
            }else if(numbers[i] == biggestNumber){

                positionBiggest.push(i);

            }
    
        }

        answer = `De los numeros ingresados el mas grande es ${biggestNumber}, en la(s) posicion(es) ${positionBiggest}`;

    }

    return answer;
}
module.exports ={

    askData,
    arrayNumbers,
    biggest
}
