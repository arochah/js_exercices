
//Leer tres numeros enteros y determinar si el último digito. de los tres numeros es igual. 
askData = function (number1, number2, number3){

    let out;

    if (typeof(number1) == "number" && typeof(number2) == "number" && typeof(number3) == "number"){

        out = [number1, number2, number3];

    }else{
        out = false;
    }

    return out;

}

areDigitiesEqual = function (numbers){

    let digit1 = numbers[0] % 10;
    let digit2 = numbers[1] % 10;
    let digit3 = numbers[2] % 10;

    let resultEval;

    if (digit1 == digit2 && digit1 == digit3){

        resultEval = `Los ultimos digitos de ${numbers[0]}, ${numbers[1]} y ${numbers[2]} SON iguales`;

    }else{
        resultEval = `Los ultimos digitos de ${numbers[0]}, ${numbers[1]} y ${numbers[2]} NO son iguales`
    }

    return resultEval;

}

theCompleteProcessOfThis = function (number1, number2, number3){

    let result;

    let theNumbers = askData(number1, number2, number3);



    if ( typeof(theNumbers) != "object" ){

        result = `Los valores ingresados, o almenos uno de ellos no es un valor numerico.`

    }else{

        result = areDigitiesEqual(theNumbers);

    }

    return result;

}

module.exports = {

    askData,
    areDigitiesEqual,
    theCompleteProcessOfThis
}
