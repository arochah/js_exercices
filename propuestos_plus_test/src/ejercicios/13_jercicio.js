
//Generar todas las tablas de multiplicar del 1 al 10

tableOneToTen = function (){

    for(let i = 1; i < 11 ; i++){

        console.log(`La Tabla Del ${i}`)
    
        for(let j = 1; j < 11 ; j++){
    
            console.log(`${i} X ${j} = ${(i * j)}`)
    
        }
    
        console.log(`<------------>`)
    }

    return `Estas fueron las tablas de multiplicar del 1 al 10`

}

module.exports = {
    tableOneToTen
}

