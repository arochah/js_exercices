
const ejercicio14 = require('../ejercicios/14_ejercicio')

describe('test a funcion que determina si el valor dado es un numero, y lo retorna', () => {

    test('el valor ingresado es un numero', () => {

        let number = 21

        let result = ejercicio14.askData(number)

        expect(result).toBe(number)
    })

    test('el valor ingresado no es un nuemro', () => {
        
        let number = 'no soy un numero'

        let result = ejercicio14.askData(number)

        expect(result).toBeFalsy()
    });
})

describe('test a funcion que muestra la tabal de multiplicar del nuemro dados', () => {

    test('se ingresa un valor que no es numerico', () => {

        let number = 'no soy un numero'

        let result = ejercicio14.tableOfOneNumber(number)

        expect(result).toBe(`Asegurese de ingresar un numero`)
    })


    test('se ingresa un valor numerico', () => {

        //sigo sin estar seguro de como evaluarla, pero aun que tengo idea el tiempo apremia :S

        let number = 22

        let result = ejercicio14.tableOfOneNumber(number)

        expect(result).toBe(`Gracias por usar este generador de tablas de multiplicar inprovisado`)
    })

})