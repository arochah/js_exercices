
const ejercicio4 = require('../ejercicios/4_ejercicio')

describe('test a funcion que determian si un numero es primo o no', ()=>{

    test('determina que el numero es primo', () => {
        
        let number = 17
        result = ejercicio4.isPrime(number)

        expect(result).toBeTruthy()
    });



    test('determina que el numero es primo', () => {
        
        let number = 17
        result = ejercicio4.isPrime(number)

        expect(result).toBeTruthy()
    });


    test('determina que el numero no es primo', () => {
        
        let number = 18
        result = ejercicio4.isPrime(number)

        expect(result).toBeFalsy()
    });
})

describe('tes a funcion que retorna mensaje de si el numero de 2 digitos es primo', () => {
    test('el numeor es primo, de 2 digitos y menor a 20', () => {
        
        let number = 17
        result = ejercicio4.primeNumberDowntwenty(number)

        expect(result).toBe(`El numero ${number} es primo`)
    });

    test('el numeor no es primo, de 2 digitos y menor a 20', () => {
        
        let number = 18
        result = ejercicio4.primeNumberDowntwenty(number)

        expect(result).toBe(`El numero ${number} no es primo`)
    });


    test('el valor ingresado es texto o un valor que no es de 2 cifras y menor a 20', () => {
        
        let number = 'soy texto'
        result = ejercicio4.primeNumberDowntwenty(number)

        expect(result).toBe(`Se debe ingresar un numero positivo menor que 20 y mayor que 9`)
    });
})