
const ejercicio9 = require('../ejercicios/9_ejercicio')

describe('test a funcion que determina si los 3 valores que recibe son numeros',() =>{

    test('se entregan 3 valores nuemricos y retorna un arreglo de estso', () =>{

        let number1 = 36
        let number2 = 41
        let number3 = 62
    
        let result = ejercicio9.askData(number1, number2, number3)
    
        expect(result).toEqual([36, 41, 62])
    })

    test('uno de los valores que se entregan no es nuemrico', () =>{

        let number1 = 'no soy un numero'
        let number2 = 41
        let number3 = 62
    
        let result = ejercicio9.askData(number1, number2, number3)
    
        expect(result).toBeFalsy()
    })

})


describe('test a funcion que recibe un arreglo de 3 nuemros ', () =>{

    test('se recibe un arreglo de 3 numeros con sus 3 ultimos dijitos iguales', () =>{

        let array = [422, 812, 362]

        let result = ejercicio9.areDigitiesEqual(array)

        expect(result).toBe(`Los ultimos digitos de ${array[0]}, ${array[1]} y ${array[2]} SON iguales`)
    })


    test('se recibe un arreglo de 3 numeros con sus 3 ultimos dijitos no son iguales', () =>{

        let array = [421, 813, 362]

        let result = ejercicio9.areDigitiesEqual(array)

        expect(result).toBe(`Los ultimos digitos de ${array[0]}, ${array[1]} y ${array[2]} NO son iguales`)
    })
})


describe('test a funcion que usa askData y areDigitiesEqual para proceso global de "ultimos digitos iguales"', () => {

    test('recibe 3 numeros con sus ultimos 3 digitos iguales', ()=> {

        let number1 = 36
        let number2 = 46
        let number3 = 66

        let result = ejercicio9.theCompleteProcessOfThis(number1, number2, number3)

        expect(result).toBe(`Los ultimos digitos de ${number1}, ${number2} y ${number3} SON iguales`)
    })


    test('recibe 3 numeros con sus ultimos 3 digitos iguales', ()=> {

        let number1 = 31
        let number2 = 46
        let number3 = 67

        let result = ejercicio9.theCompleteProcessOfThis(number1, number2, number3)

        expect(result).toBe(`Los ultimos digitos de ${number1}, ${number2} y ${number3} NO son iguales`)
    })


    test('uno de los valores ingresados no es un numero', () => {
        
        let number1 = 'soy texto'
        let number2 = 46
        let number3 = 67

        let result = ejercicio9.theCompleteProcessOfThis(number1, number2, number3)

        expect(result).toBe(`Los valores ingresados, o almenos uno de ellos no es un valor numerico.`)
    });
})