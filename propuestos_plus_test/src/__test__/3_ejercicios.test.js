
const ejercicio3 = require('../ejercicios/3_ejercicio')

describe('Test de programa que determina si los 2 digitos de un numero son pares', () => {

    test('Los 2 digitos son pares', () => {

        let number = 46;

        result = ejercicio3.twoDigitisEven(number)

        expect(result).toEqual(`Los 2 digitos de 46 son pares!`)
    })


    test('Los 2 digitos  no son pares', () => {

        let number = 45;

        result = ejercicio3.twoDigitisEven(number)

        expect(result).toEqual(`El numero 45 no esta formado por digitos pares`)
    })


    test('Se ingresa texto', () => {

        let number = 'Soy texto';

        result = ejercicio3.twoDigitisEven(number)

        expect(result).toEqual(`El valor ingresado debe ser un numero de 2 cifras`)
    })


    test('Se ingresa un numero de 3 cifras o mas', () => {

        let number = 340;

        result = ejercicio3.twoDigitisEven(number)

        expect(result).toEqual(`El valor ingresado debe ser un numero de 2 cifras`)
    })

    test('Se ingresa un numero de 1 cifra', () => {

        let number = 5;

        result = ejercicio3.twoDigitisEven(number)

        expect(result).toEqual(`El valor ingresado debe ser un numero de 2 cifras`)
    })
})