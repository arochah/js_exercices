
const ejercicio1 = require('../ejercicios/1_ejercicio')

describe('Testeo de ejercicio que determina si un entero es positivo o negativo.', () => {

    test('Determina que el numero es positivo', () => {

        let numero = 5

        let result = ejercicio1.positiveOrNegative(numero)

        expect(result).toBe(`El numero 5 es positivo!`)
    })

    test('Determina que el numero es negativo', () => {

        let numero = -5

        let result = ejercicio1.positiveOrNegative(numero)

        expect(result).toBe(`El numero -5 es negativo!`)
    })

    test('Se ingreso una letra', () => {

        let numero = 'soy texto'

        let result = ejercicio1.positiveOrNegative(numero)

        expect(result).toBe(`Se debe ingresar un numero`)
    })
})