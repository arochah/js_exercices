
const ejercicio7 = require('../ejercicios/7_ejercicio')

describe('test a funcion que toma numero de 2 digitos y retorna sus digitos en un arreglo', ()=>{

    test('retorna los 2 digitos de un numero positivo', ()=>{

        let number = 27
        let result = ejercicio7.digits(number)

        expect(result).toEqual([2, 7])
    })

    test('retorna los 2 digitos de un numero negativo', ()=>{

        let number = -15
        let result = ejercicio7.digits(number)

        expect(result).toEqual([-1, -5])
    })
})

describe('test a funcion que determina si 2 digitos son iguales', ()=>{

    test('mensaje que los dijitos del numero de 2 cifras son iguales', ()=>{

        let number = 33
        let result = ejercicio7.equalDigitis(number)

        expect(result).toBe(`Los digitos de ${number} son iguales`)
    })


    test('mensaje que los dijitos del numero de 2 cifras no son iguales', ()=>{

        let number = 34
        let result = ejercicio7.equalDigitis(number)

        expect(result).toBe(`Los digitos de ${number} no son iguales`)
    })


    test('mensaje que el valor suministrado no es un numeor de 2 cifras', ()=>{

        let number = 'soy texto pero tambien puedo ser un numero que no es de 2 cifras'
        let result = ejercicio7.equalDigitis(number)

        expect(result).toBe(`Se debe ingresar un numero de 2 cifras`)
    })
})