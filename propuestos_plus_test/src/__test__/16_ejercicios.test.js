
const ejercicio16 = require('../ejercicios/16_ejercicio')

describe('test a funcion que determina si el valor dado es un numero, y lo retorna', () => {

    test('el valor ingresado es un numero', () => {

        let number = 21

        let result = ejercicio16.askData(number)

        expect(result).toBe(number)
    })

    test('el valor ingresado no es un nuemro', () => {
        
        let number = 'no soy un numero'

        let result = ejercicio16.askData(number)

        expect(result).toBeFalsy()
    });
})

describe('test a funcion que recibe n cantidad de nuemros y retorna un arreglo de estos', () =>{

    test('se ingresa una serie de nuemros', () =>{

        let number1 = 22
        let number2 = 5
        let number3 = 11
        let number4 = 53

        let result = ejercicio16.arrayNumbers(number1, number2, number3, number4)

        expect(result).toEqual([number1,number2,number3,number4])
    })


    test('respuesta cuando uno o mas valores ingresados no son un numero', () =>{

        let number1 = 'no soy un numero'
        let number2 = 5
        let number3 = 11
        let number4 = 'yo tampoco soy un numero'

        let result = ejercicio16.arrayNumbers(number1, number2, number3, number4)

        expect(result).toBe("Uno de los valores ingresados no es un numero!!")
    })
})

describe('test a funcion que recibe un arreglo de nuemros y retorna un arreglo con las posiciones de los numeros que termian en 0', ()=>{

    test('cunado recibe un strin retorna este mismo', ()=>{

        let number = 'esto es texto!!'

        let result = ejercicio16.lastNumerCero(number)

        expect(result).toBe('esto es texto!!')
    })

    test('cunado recibe un arreglo de numeros retorna un texto con la posicion de los numeros que terminan en 0', ()=>{

        let number = [30, 4, 6, 10, 9]

        let result = ejercicio16.lastNumerCero(number)

        expect(result).toEqual(`Las posiciones de los numeros que terminan en 0 son 0,3`)
    })
})