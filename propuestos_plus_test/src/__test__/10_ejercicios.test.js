
const ejercicio10 = require('../ejercicios/10_ejercicio')


describe('test a funcion que verifica si se le ingresan 2 numeros y regresa un arreglo de estos', () => {

    test('recibe 2 numeros y retorna un arreglo de estos', () => {

        let number1 = 22
        let number2 = 213

        let result = ejercicio10.askData(number1, number2)

        expect(result).toEqual([number1, number2])

    })


    test('uno de los valores dados no es un numero', () => {

        let number1 = 'no soy un numero'
        let number2 = 213

        let result = ejercicio10.askData(number1, number2)

        expect(result).toBeFalsy()

    })
})

describe('test a funcion que recibe un numero y su pocible divisor', ()=>{

    test('si es divisor del numero', () => {

        let number = 22
        let divisor = 2 

        let result = ejercicio10.isDivisor(number, divisor)

        expect(result).toBe(` ES divisor de ${number}`)
    });


    test('no es divisor del numero', () => {

        let number = 22
        let divisor = 5 

        let result = ejercicio10.isDivisor(number, divisor)

        expect(result).toBe(` NO es divisor de ${number}`)
    });
})


describe('test a funcion que recibe un arreglo de 2 numeros y usando isDivisor muestra si al diferencia de estos es un numero divisor de uno de estos', ()=>{

    test('la diferencia de los numeros es divisor de los 2', ()=>{

        let numbers = [24, 28]
        let dif = Math.abs(numbers[0] - numbers[1])

        let result = ejercicio10.testOfCondition(numbers)

        expect(result).toBe(`La diferencia de los numeros es ${dif}, ES divisor de ${numbers[0]} y ES divisor de ${numbers[1]}`)
    })



    test('la diferencia de los numeros  no es divisor de los 2', ()=>{

        let numbers = [23, 27]
        let dif = Math.abs(numbers[0] - numbers[1])

        let result = ejercicio10.testOfCondition(numbers)

        expect(result).toBe(`La diferencia de los numeros es ${dif}, NO es divisor de ${numbers[0]} y NO es divisor de ${numbers[1]}`)
    })
})


describe('test a funcion que verifica si se estan pasando valores numericos a testOfCondition usando askData', () => {

    test('se ingresan 2 numeros, la diferencia de estos es divisor exacto de ellos', () => {

        let number1 = 24
        let number2 = 28
        let dif = Math.abs(number1 - number2)
    
        let result = ejercicio10.isTheCase(number1, number2)

        expect(result).toBe(`La diferencia de los numeros es ${dif}, ES divisor de ${number1} y ES divisor de ${number2}`)

    })


    test('se ingresan 2 numeros, la diferencia de estos no es divisor exacto de ellos', () => {

        let number1 = 23
        let number2 = 27
        let dif = Math.abs(number1 - number2)
    
        let result = ejercicio10.isTheCase(number1, number2)

        expect(result).toBe(`La diferencia de los numeros es ${dif}, NO es divisor de ${number1} y NO es divisor de ${number2}`)

    })


    test('uno de los valores ingresados no es un numero', () => {

        let number1 = 'no soy un numero'
        let number2 = 27
    
        let result = ejercicio10.isTheCase(number1, number2)

        expect(result).toBe(`Los valores ingresados, o almenos uno de ellos no es un valor numerico.`)

    })

})