
const ejercicio15 = require('../ejercicios/15_ejercicio')

describe('test a funcion que determina si el valor dado es un numero, y lo retorna', () => {

    test('el valor ingresado es un numero', () => {

        let number = 21

        let result = ejercicio15.askData(number)

        expect(result).toBe(number)
    })

    test('el valor ingresado no es un nuemro', () => {
        
        let number = 'no soy un numero'

        let result = ejercicio15.askData(number)

        expect(result).toBeFalsy()
    });
})

describe('test a funcion que recibe n cantidad de nuemros y retorna un arreglo de estos', () =>{

    test('se ingresa una serie de nuemros', () =>{

        let number1 = 22
        let number2 = 5
        let number3 = 11
        let number4 = 53

        let result = ejercicio15.arrayNumbers(number1, number2, number3, number4)

        expect(result).toEqual([number1,number2,number3,number4])
    })


    test('respuesta cuando uno o mas valores ingresados no son un numero', () =>{

        let number1 = 'no soy un numero'
        let number2 = 5
        let number3 = 11
        let number4 = 'yo tampoco soy un numero'

        let result = ejercicio15.arrayNumbers(number1, number2, number3, number4)

        expect(result).toBe("Uno de los valores ingresados no es un numero!!")
    })
})

describe("test a funcion que determina la posicion del numero mayor en un arreglo de numeros, recibe la respusta de arrayNumbers", () => {

    test("cuando recibe un string retorna este mismo como respuesta", () => {

        let numbers = "Uno de los valores ingresados no es un numero!!"

        let result = ejercicio15.biggest(numbers)

        expect(result).toBe("Uno de los valores ingresados no es un numero!!")
    })


    test('se ingresa un arreglo de nuemros, retornara un mensaje con al posicion del mayor', ()=>{

        let numbers = [1, 2, 3, 11, 5, 6]

        let result = ejercicio15.biggest(numbers)

        expect(result).toBe(`De los numeros ingresados el mas grande es 11, en la posicion 3`)
    })
})