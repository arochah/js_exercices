
const ejercicio2 = require('../ejercicios/2_ejercicio')

describe('Testeo Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos', () => {

    test('suma los digitos de un numero de 2 cifras', () => {

        let number = 22
        let result = ejercicio2.sumTwoDigitis(number)

        expect(result).toEqual(4)
    })



    test('Se ingresa un nuemro de 1 cifra', () => {

        let number = 1
        let result = ejercicio2.sumTwoDigitis(number)

        expect(result).toEqual(`El valor ingresado debe ser un numero de 2 cifras`)
    })



    test('Se ingresa un nuemro de 3 cifra', () => {

        let number = 100
        let result = ejercicio2.sumTwoDigitis(number)

        expect(result).toEqual(`El valor ingresado debe ser un numero de 2 cifras`)
    })


    test('Se ingresa texto', () => {

        let number = 'soy texto'
        let result = ejercicio2.sumTwoDigitis(number)

        expect(result).toEqual(`El valor ingresado debe ser un numero de 2 cifras`)
    })
})