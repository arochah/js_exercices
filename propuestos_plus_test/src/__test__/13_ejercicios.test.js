
const ejercicio13 = require('../ejercicios/13_jercicio')

describe('test a funcion que muestra la tabla de multiplicar del 1 al 10', () => {

    //tengo dudas sobre como implementar adecuadamente el test a este programa

    test('', () => {

        let result = ejercicio13.tableOneToTen()

        expect(result).toBe(`Estas fueron las tablas de multiplicar del 1 al 10`)

    })
})