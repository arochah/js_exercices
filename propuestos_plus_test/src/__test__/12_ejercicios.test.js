
const ejercicio12 = require('../ejercicios/12_ejercicio')

describe('test a funcion que determina si el valor dado es un numero, y lo retorna', () => {

    test('el valor ingresado es un numero', () => {

        let number = 21

        let result = ejercicio12.askData(number)

        expect(result).toBe(number)
    })

    test('el valor ingresado no es un nuemro', () => {
        
        let number = 'no soy un numero'

        let result = ejercicio12.askData(number)

        expect(result).toBeFalsy()
    });
})


describe('test a funcion que toma un numero y regresa un arreglo de sus digitos como string', () => {

    //La funcion askData no permite que a esta llegue un valor que no sea numerico, por lo que esta posibilidad no sera evaluada

    test('se le ingresa un numero de 4 cifras', () => {

        let number = 2941

        let result = ejercicio12.splitTheNumber(number)

        expect(result).toEqual(['2', '9', '4', '1'])
    })

    test('se le ingresa un numero de 1 cifras', () => {

        let number = 2

        let result = ejercicio12.splitTheNumber(number)

        expect(result).toEqual(['2'])
    })

    test('se le ingresa un numero de 3 cifras', () => {

        let number = 319

        let result = ejercicio12.splitTheNumber(number)

        expect(result).toEqual(['3', '1', '9'])
    })
})


describe('test a funcion que recibe un arreglo de numeros como string y determina cunatas veces esta el nuemro 1', () => {

    //la funcion askData no permite que a esta lleguen valores en el arreglo que no sean string numericos
    //por o que no se evaluara dicha posibilidad
    test('se ingresa un arreglo que no contiene el nuemro 1', ()=>{

        let array = ['4', '5', '2', '5'];

        let result = ejercicio12.isOne(array)

        expect(result).toBe(0)
    })

    test('se ingresa un arreglo que contiene el nuemro 1 tres veces', ()=>{

        let array = ['4', '1', '1', '5', '4', '1', '6'];

        let result = ejercicio12.isOne(array)

        expect(result).toBe(3)
    })

    test('se ingresa un arreglo que contiene el nuemro 2 tres veces', ()=>{

        let array = ['4', '1', '5', '4', '1', '6'];

        let result = ejercicio12.isOne(array)

        expect(result).toBe(2)
    })
})


describe('test a funcion que maneja el proceso general usando askData, isOne, splitTheNumbers', () => {

    test('se ingresa un numero que no posee 1', () => {

        let number = 294

        let result = ejercicio12.generalProcess(number)

        expect(result).toBe(`En el numero ${number} tiene el numero 1 la siguiente cantidad de veces : 0`)

    })


    test('se ingresa un numero que posee 1 dos veces', () => {

        let number = 1314

        let result = ejercicio12.generalProcess(number)

        expect(result).toBe(`En el numero ${number} tiene el numero 1 la siguiente cantidad de veces : 2`)

    })


    test('se ingresa un valor que no es nuemrico', () => {

        let number = 'no soy un numero'

        let result = ejercicio12.generalProcess(number)

        expect(result).toBe(`El valor ingresado no es un numero`)

    })
})