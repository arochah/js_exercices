
const ejercicio11 = require('../ejercicios/11_ejercicio')


describe('test a funcion que determina si el valor dado es nu numero, y lo retorna', () => {

    test('el valor ingresado es un numero', () => {

        let number = 21

        let result = ejercicio11.askData(number)

        expect(result).toBe(number)
    })

    test('el valor ingresado no es un nuemro', () => {
        
        let number = 'no soy un numero'

        let result = ejercicio11.askData(number)

        expect(result).toBeFalsy()
    });
})


describe('test a funcion que toma un numero y regresa un arreglo de sus digitos como string', () => {

    //La funcion askData no permite que a esta llegue un valor que no sea numerico, por lo que esta posibilidad no sera evaluada

    test('se le ingresa un numero de 4 cifras', () => {

        let number = 2941

        let result = ejercicio11.splitTheNumber(number)

        expect(result).toEqual(['2', '9', '4', '1'])
    })

    test('se le ingresa un numero de 1 cifras', () => {

        let number = 2

        let result = ejercicio11.splitTheNumber(number)

        expect(result).toEqual(['2'])
    })

    test('se le ingresa un numero de 3 cifras', () => {

        let number = 319

        let result = ejercicio11.splitTheNumber(number)

        expect(result).toEqual(['3', '1', '9'])
    })
})


describe('test a funcion que suma numeros en tipo string pasadoos en un arreglo ', () => {

    //la funcion askData no permite que a esta lleguen valores en el arreglo que no sean string numericos
    //por o que no se evaluara dicha posibilidad

    test('se ingresa un arreglo con tres valores', () => {

        let arreglo = ['2', '3', '4']

        let result = ejercicio11.sumDigits(arreglo)

        expect(result).toEqual(9)
    })

    test('se ingresa un arreglo con cuatro valores', () => {

        let arreglo = ['2', '3', '4', '2']

        let result = ejercicio11.sumDigits(arreglo)

        expect(result).toEqual(11)
    })


    test('se ingresa un arreglo con un valores', () => {

        let arreglo = ['2']

        let result = ejercicio11.sumDigits(arreglo)

        expect(result).toEqual(2)
    })
})


describe('test a funcion que maneja el proceso general usando askData, sumDigits, splitTheNumbers', () => {

    test('se ingresa un numero de 3 cifras', () => {

        let number = 294

        let result = ejercicio11.generalProcess(number)

        expect(result).toBe(`Los digitos de ${number} suman : 15`)

    })


    test('se ingresa un numero de 4 cifras', () => {

        let number = 2394

        let result = ejercicio11.generalProcess(number)

        expect(result).toBe(`Los digitos de ${number} suman : 18`)

    })


    test('se ingresa un valor que no es nuemrico', () => {

        let number = 'no soy un numero'

        let result = ejercicio11.generalProcess(number)

        expect(result).toBe(`El valor ingresado no es un numero`)

    })
})