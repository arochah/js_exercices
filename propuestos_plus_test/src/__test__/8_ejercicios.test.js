
const ejercicio8 = require('../ejercicios/8_ejercicio')


describe('test a funcion que toma un numero de 4 digitos y retorna un arreglocon sus digitos en orden', ()=>{

    test('toma un numero de 4 digitos y retorna un arreglo de los digitos', () => {
        let number = 4321
        let result = ejercicio8.splitNumberFourDigities(number)
    
        expect(result).toEqual(['4', '3', '2', '1'])
    });

})

describe('test a funcion que toma un nuemor de 4 digitos dice si el segundo y tercero sin iguales', ()=>{

    test('toma un numero de 4 cifras con el segundo y tercer digito iguales', ()=>{

        let number = 1226
        let result = ejercicio8.someEqualDigities(number)

        expect(result).toBe(`En ${number}, el penultimo digito es igual al segundo`)
    })


    test('toma un numero de 4 cifras deonde el segundo y tercer digito no son iguales', ()=>{

        let number = 1326
        let result = ejercicio8.someEqualDigities(number)

        expect(result).toBe(`En ${number}, el penultimo digito no es igual al segundo`)
    })


    test('se toma un valor que no es un numeor de 4 cifras', ()=>{

        let number = 'soy texto o un nuemro que no es de 4 cifras'
        let result = ejercicio8.someEqualDigities(number)

        expect(result).toBe(`Se debe ingresar un numero de 4 cifras`)
    })
})