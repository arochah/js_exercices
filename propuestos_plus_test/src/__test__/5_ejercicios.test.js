
const ejercicio5 =require('../ejercicios/5_ejercicio')

describe('test a funcion que determina si un numero es primo', ()=>{

    test('determina que el numero es primo', () => {
        
        let number = 17
        result = ejercicio5.isPrime(number)

        expect(result).toBeTruthy()
    });


    test('determina que el numero no es primo', () => {
        
        let number = 18
        result = ejercicio5.isPrime(number)

        expect(result).toBeFalsy()
    });

})


describe('test a funcion que retorna mensaje para numero de 2 digitos primo', () => {
    test('numero de 2 digitos positivo que es primo', () => {

        let number = 37
        result = ejercicio5.primeNumberTwoDigitis(number)

        expect(result).toBe(`El numero ${number} es primo`)
    });


    test('numero de 2 digitos positivo que no es primo', () => {

        let number = 40
        result = ejercicio5.primeNumberTwoDigitis(number)

        expect(result).toBe(`El numero ${number} no es primo`)
    });


    test('numero negativo', () => {

        let number = -31
        result = ejercicio5.primeNumberTwoDigitis(number)

        expect(result).toBe(`El numero ${number} es negativo, pero no es primo ya que la definicion de numero primo no incluye a los negativos!`)
    });


    test('un valor que no es de 2 digitos o texto', () => {

        let number = 'Soy texto, pero podria ser un numero que no es de 2 cifras'
        result = ejercicio5.primeNumberTwoDigitis(number)

        expect(result).toBe(`Se debe ingresar un numero positivo mayor que 1 y menor que 100 `)
    });
})