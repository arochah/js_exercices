
const ejercicio6 = require('../ejercicios/6_ejercicio')

describe('test a programa que leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro', ()=>{

    test('recibe un numero de 2 cifras positivo y retorna un arreglo de las cifras', ()=>{

        let number = 81
        let result = ejercicio6.digits(number)

        expect(result).toEqual([8, 1])
    })


    test('recibe un numero de 2 cifras negativo y retorna un arreglo de las cifras', ()=>{

        let number = -81
        let result = ejercicio6.digits(number)

        expect(result).toEqual([-8, -1])
    })
})

describe('test a funcion que retorna mensaje para numero de 2 digitos que sus cifras sean multiplos entre si', () => {

    test('retorna que el primer digito del numero es multiplo del segundo', ()=>{

        let number = 81
        
        let result = ejercicio6.isMultiple(number)

        expect(result).toBe(`En el numero ${number}, 8 es multiplo de 1`)
    })


    test('retorna que el segundo numero es multiplo del primero', ()=>{

        let number = 24
        
        let result = ejercicio6.isMultiple(number)

        expect(result).toBe(`En el numero ${number}, 4 es multiplo de 2`)
    })


    test('retorna que el segundo numero es multiplo del primero', ()=>{

        let number = 24
        
        let result = ejercicio6.isMultiple(number)

        expect(result).toBe(`En el numero ${number}, 4 es multiplo de 2`)
    })


    test('retorna que los 2 digitos del numero no son multiplos entre si', ()=>{

        let number = 25
        
        let result = ejercicio6.isMultiple(number)

        expect(result).toBe(`los digitos que conforman ${number} no son multiplos entre si`)
    })


    test('retorna que el valor ingresado no es un numero de 2 cifras', ()=>{

        let number = 'soy texto pero podria ser un numero que no es de 2 cifras'
        
        let result = ejercicio6.isMultiple(number)

        expect(result).toBe(`Se debe ingresar un numero de 2 cifras`)
    })
})